import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { ShoppingTwist2020SharedModule } from 'app/shared/shared.module';
import { ShoppingTwist2020CoreModule } from 'app/core/core.module';
import { ShoppingTwist2020AppRoutingModule } from './app-routing.module';
import { ShoppingTwist2020HomeModule } from './home/home.module';
import { ShoppingTwist2020EntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    ShoppingTwist2020SharedModule,
    ShoppingTwist2020CoreModule,
    ShoppingTwist2020HomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    ShoppingTwist2020EntityModule,
    ShoppingTwist2020AppRoutingModule
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, FooterComponent],
  bootstrap: [MainComponent]
})
export class ShoppingTwist2020AppModule {}
