import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ShoppingTwist2020SharedModule } from 'app/shared/shared.module';
import { ShoppingListComponent } from './shopping-list.component';
import { ShoppingListDetailComponent } from './shopping-list-detail.component';
import { ShoppingListUpdateComponent } from './shopping-list-update.component';
import { ShoppingListDeleteDialogComponent } from './shopping-list-delete-dialog.component';
import { shoppingListRoute } from './shopping-list.route';

@NgModule({
  imports: [ShoppingTwist2020SharedModule, RouterModule.forChild(shoppingListRoute)],
  declarations: [ShoppingListComponent, ShoppingListDetailComponent, ShoppingListUpdateComponent, ShoppingListDeleteDialogComponent],
  entryComponents: [ShoppingListDeleteDialogComponent]
})
export class ShoppingTwist2020ShoppingListModule {}
