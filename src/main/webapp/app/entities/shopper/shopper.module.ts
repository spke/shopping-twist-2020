import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ShoppingTwist2020SharedModule } from 'app/shared/shared.module';
import { ShopperComponent } from './shopper.component';
import { ShopperDetailComponent } from './shopper-detail.component';
import { ShopperUpdateComponent } from './shopper-update.component';
import { ShopperDeleteDialogComponent } from './shopper-delete-dialog.component';
import { shopperRoute } from './shopper.route';

@NgModule({
  imports: [ShoppingTwist2020SharedModule, RouterModule.forChild(shopperRoute)],
  declarations: [ShopperComponent, ShopperDetailComponent, ShopperUpdateComponent, ShopperDeleteDialogComponent],
  entryComponents: [ShopperDeleteDialogComponent]
})
export class ShoppingTwist2020ShopperModule {}
