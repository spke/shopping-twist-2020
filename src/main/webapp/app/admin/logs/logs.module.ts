import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ShoppingTwist2020SharedModule } from 'app/shared/shared.module';

import { LogsComponent } from './logs.component';

import { logsRoute } from './logs.route';

@NgModule({
  imports: [ShoppingTwist2020SharedModule, RouterModule.forChild([logsRoute])],
  declarations: [LogsComponent]
})
export class LogsModule {}
